import re
import openpyxl
import pprint


def is_email(email):
    try:
        regex = r"(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])"
        return re.match(regex, email)
    except TypeError:
        return False


def validation_id(id):
    try:
        int(id)
        return True
    except:
        return False


def validation_sales(v_sales):
    try:
        float(v_sales.replace(',', '.'))
        return True
    except:
        return False




class ReadFile:

    def __init__(self, path):
        self.path = path

    def read_file(self):
        wb_obj = openpyxl.load_workbook(self.path)
        sheet_obj = wb_obj.active
        first_row = []

        for col in range(sheet_obj.max_column):
            first_row.append(sheet_obj.cell(1, col + 1).value)
        data = []

        for row in range(2, sheet_obj.max_row + 1):
            elm = {}
            for col in range(sheet_obj.max_column):
                elm[first_row[col]] = sheet_obj.cell(row, col + 1).value
            data.append(elm)
        return data

    def clean_data(self, data):
        clean_data = []
        trash_data = []
        for item in data:
            if (validation_id(item.get('row_id')) and is_email(item.get('email'))):
                validation_sales(item.get('sales'))
                clean_data.append(item)
            else:
                trash_data.append(item)
        return trash_data, clean_data

    def separar_ship_mode(self, data):
        tipo_ship = {
            'first_class': [],
            'second_class': [],
            'standard_class': [],
            'same_day': [],
        }
        for item in data:
            if (item.get('ship_mode') == 'Standard Class'):
                tipo_ship['standard_class'].append(item)
            if (item.get('ship_mode') == 'First Class'):
                tipo_ship['first_class'].append(item)
            if (item.get('ship_mode') == 'Second Class'):
                tipo_ship['second_class'].append(item)
            if (item.get('ship_mode') == 'Same Class'):
                tipo_ship['same_day'].append(item)

        return tipo_ship


def main():
    pi = pprint.PrettyPrinter(indent=2)
    test = ReadFile('/home/lsv/Escritorio/Entorno virtual/python-excel/data/Sample - Superstore.xlsx')
    # QUITE SUS RESPECTIVA COMILLAS DE LA LISTA QUE DESEE VER.
    """
    print('*********DATOS LIMPIOS**********')
    pi.pprint(test.clean_data(test.read_file())[1])
    print(' ')
    print('*********DATOS SUCIOS**********')
    pi.pprint(test.clean_data(test.read_file())[0])
    print(' ')
    print('*********DATOS ORDENADOS POR SHIP MODE**********')
    ship_mode_datos = test.separar_ship_mode(test.clean_data(test.read_file())[1])
    pi.pprint(ship_mode_datos)
    """
    print(len(test.clean_data(test.read_file())[1]))



if __name__ == '__main__':
    main()


